<!DOCTYPE html>
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Universidad de Ciencias Empresariales</title>
   <link rel="stylesheet" type="text/css" href="./styles/main.scss"/>
   <script src="https://kit.fontawesome.com/c672bf2245.js" crossorigin="anonymous"></script>
   <title>Menú navegacion</title> <link rel="stylesheet" href="./styles/estilos.scss">
</head>

<h2> Deseo informacion</h2>
<div class="conte">
  <form action="action_page.php">

    <label for="fname">First Name</label>
    <input type="text" id="fname" name="firstname" placeholder="Your name..">

    <label for="lname">Last Name</label>
    <input type="text" id="lname" name="lastname" placeholder="Your last name..">

    <label for="country">Country</label>
    <select id="country" name="country">
      <option value="australia">Australia</option>
      <option value="canada">Canada</option>
      <option value="usa">Costa Rica</option>
    </select>

    <label for="subject">Subject</label>
    <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px"></textarea>

    <input type="submit" value="Submit">

  </form>
</div>
 

<footer>
    &#169; 2023 Hecho por Yadith Rodriguez  |
   <nav class="redes">
     <a href="https://www.facebook.com" target="_blank"><i class="fab fa-facebook" aria-hidden="true"></i></a>
     <a href="https://www.instagram.com" target="_blank"><i class="fab fa-instagram" aria-hidden="true"></i></a>
     <a href="https://twitter.com" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i></a>
     <a href="https://www.linkedin.com" target="_blank"><i class="fab fa-linkedin" aria-hidden="true"></i></a>
   </nav> 
</footer>
  


</html> 