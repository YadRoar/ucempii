<?php require_once "parte_superior.php"?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.container {
  position: relative;
  width: 50%;
}

.image {
  display: block;
  width: 100%;
  height: auto;
}

.overlay {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  opacity: 0;
  transition: .5s ease;
  background-color: #008CBA;
}

.container:hover .overlay {
  opacity: 1;
}

.text {
  color: white;
  font-size: 20px;
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  text-align: center;
}
</style>
</head>
<body>
<br>
<br>
<br>

<h2>Ingenieria en Sistemas</h2>


<div class="container">
  <img src="https://www.ucem.ac.cr/wp-content/uploads/2023/03/ingeniero-de-sistemas.jpg" alt="Avatar" class="image">
  <div class="overlay">
    <div class="text">Como futuro Ingeniero en Sistemas generarás productos tecnológicos que buscan la solución de problemas en ámbitos cotidianos, <br>
    científicos e industriales y en los que integra competencias y habilidades de las ciencias computacionales, la ingeniería de software y la infraestructura computacional..</div>
  </div>
</div>

<h2>Ingenieria Industrial</h2>
<br>
<br>

<div class="container">
  <img src="https://www.ucem.ac.cr/wp-content/uploads/2023/03/Ingenieros-diriguiendo-un-proyecto-1536x864-1.jpg" alt="Avatar" class="image">
  <div class="overlay">
    <div class="text">Se distingue por liderar procesos de cambio en un entorno globalizado y dinámico; y ser capaz de integrar herramientas metodológicas para incrementar la productividad y competitividad, aplicando la gestión de proyectos, la modelación matemática, el uso herramientas analíticas y métodos estadísticos; así como la utilización de tecnologías de información.
    </div>
  </div>
</div>
<br>
<br>
<h2>Administracion de Negocios</h2>
<br>
<br>

<div class="container">
  <img src="https://www.ucem.ac.cr/wp-content/uploads/2023/03/contadores-1.jpg" alt="Avatar" class="image">
  <div class="overlay">
    <div class="text">Formula modelos de gestión estratégica a partir del análisis del entorno, con enfoque sistémico, visión de futuro e incorporando analítica de datos que apoye la toma de decisiones, mediante el diseño de plataformas de negocio que integren los recursos y capacidades estratégicos en la creación de valor para los actores involucrados
    </div>
  </div>
</div>
<br>
<br>

<h2>Contaduria</h2>
<br>
<br>
<div class="container">
  <img src="https://www.ucem.ac.cr/wp-content/uploads/2023/03/administrador-de-negocios.jpg" alt="Avatar" class="image">
  <div class="overlay">
    <div class="text">Como  experto en información contable y con una sólida preparación en finanzas, cuenta con la competencia para evaluar y  opinar sobre la calidad y confiabilidad de la información financiera-fiscal que generan las organizaciones, de acuerdo con la normatividad internacional en el ámbito financiero,  fiscal y de auditoría.
    </div>
  </div>
</div>


</body>
</html>
