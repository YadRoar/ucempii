<?php require_once "parte_superior.php"?>

<body>
  <!-- Slideshow container -->
  <div class="slideshow-container">
   <!-- Full-width images with number and caption text -->
  <div class="mySlides fade">
      <div class="numbertext">1 / 3</div>
      <img src="https://www.ucem.ac.cr/wp-content/uploads/2023/04/DSC_6191-1024x550.jpg" style="width:100%">
      <div class="text">Caption Text</div>
  </div>

  <div class="mySlides fade">
     <div class="numbertext">2 / 3</div>
     <img src="https://www.ucem.ac.cr/wp-content/uploads/2023/03/contadores-1.jpg" style="width:100%">
    <div class="text">Caption Two</div>
  </div>

 <div class="mySlides fade">
     <div class="numbertext">3 / 3</div>
     <img src="https://www.ucem.ac.cr/wp-content/uploads/2023/03/Ingenieros-diriguiendo-un-proyecto-1536x864-1.jpg" style="width:100%">
     <div class="text">Caption Three</div>
 </div>

    <!-- Next and previous buttons -->
     <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
     <a class="next" onclick="plusSlides(1)">&#10095;</a>
  </div>
   <br>

    <!-- The dots/circles -->
   <div style="text-align:center">
     <span class="dot" onclick="currentSlide(1)"></span>
     <span class="dot" onclick="currentSlide(2)"></span>
     <span class="dot" onclick="currentSlide(3)"></span>
   </div>


   <div class="timeline">
  <div class="container left">
    <div class="content">
      <h2>Abril 08, 2023</h2>
      <p>Finalizan lecciones primer cuatrimestre 2023</p>
    </div>
  </div>
  <div class="container right">
    <div class="content">
      <h2>Abril 14, 2023</h2>
      <p>Plazo para entrega de calificaciones a estudiantes del primer cuatrimestre 2023</p>
    </div>
  </div>
</div>
  
  <script src='./script/slide.js'></script>
</body>


<?php require_once "parte_inferior.php"?>